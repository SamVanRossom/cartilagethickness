% clc;
% clear;

function [out]= closestDT(Cart,Fem,OUT); 
% CAR=input('Articular Cartilage File Name : ','s');
% SUB=input('Subchondral Bone File Name : ','s');
% OUT=input('Output File Name : ','s');

disp('Start function closestDT')
% disp('Reading Articular Cartilage Surface');
% [CarSurf,CarVert,N_cart]= STL_ReadFile(CAR); 

CarSurf = Cart.F;
CarVert = Cart.V; 
N_cart = Cart.N; 

% [CarVert,CarSurf]=ReadOBJ([CAR,'.obj']);
% disp('Reading Subchondral Bone Surface');
% [SubVert,SubSurf]=ReadOBJ([SUB,'.obj']);
% [SubSurf,SubVert,N_sub]= STL_ReadFile(SUB); 

SubSurf = Fem.F; SubVert = Fem.V; N_sub = Fem.N; 

% disp('Calculating Surface Nomals');
SubEdge1=(SubVert(SubSurf(:,1),:)-SubVert(SubSurf(:,2),:));
SubEdge2=(SubVert(SubSurf(:,1),:)-SubVert(SubSurf(:,3),:));
SubSurfNorm=cross(SubEdge1,SubEdge2);

clear SubEdge1 SubEdge2 CAR SUB carFile subFile

% For every point in CarVert (cartilage surface) find index of closest point in SubVert (bone surface)
% disp('Searching The Closest Points')
ClosestPTs=dsearchn(SubVert,CarVert);

% % To plot surfaces
% plot3(CarVert(:,1), CarVert(:,2), CarVert(:,3), 'r.')
% hold on
% plot3(SubVert(:,1), SubVert(:,2), SubVert(:,3), 'b.')
% movegui('east')

% disp('Calculating Cartilage Thickness (The Closest Distance Search)')
%----------------------------------------------------------------- 
% 
%               x=[dot(h,(A-a))/dot(h,d)]d + a
% 
%       a : vertex points on cartilage surface
%       A : closest points for each 'a' on the cartilage surface
%       h : surface normals on subchondral bone surface
%       d : vector of lines which start from each 'a'
%       d=hcarFilecarFile
%
%-----------------------------------------------------------------

Bad_Points = 0;
for i=1:size(CarVert,1)
    i;
    [I,J]=find(SubSurf==ClosestPTs(i));
    % If closest point is not associated with a triangle find another point
    while isempty(I)
        SubVert(ClosestPTs(i),:) = [1000000 1000000 1000000];
        ClosestPTs(i) = dsearchn(SubVert, CarVert(i,:));
        [I,J]=find(SubSurf==ClosestPTs(i));
        Bad_Points = Bad_Points+1;
    end
    h=SubSurfNorm(I,:);   
    a=CarVert(i,:);
    A=SubVert(ClosestPTs(i),:);
    CON=(h*(A-a)')./diag(h*h');
    Dist=[h(:,1).*CON h(:,2).*CON h(:,3).*CON];
    x=[Dist(:,1)+a(1) Dist(:,2)+a(2) Dist(:,3)+a(3)];
    
    for j=1:size(I,1)
        P1=SubVert(SubSurf(I(j),1),:)';
        P2=SubVert(SubSurf(I(j),2),:)';
        P3=SubVert(SubSurf(I(j),3),:)';
        INxy=inpolygon(x(j,1),x(j,2),[P1(1) P2(1) P3(1)],[P1(2) P2(2) P3(2)]);
        INyz=inpolygon(x(j,2),x(j,3),[P1(2) P2(2) P3(2)],[P1(3) P2(3) P3(3)]);
        INzx=inpolygon(x(j,3),x(j,1),[P1(3) P2(3) P3(3)],[P1(1) P2(1) P3(1)]);
        IN(j)=INxy+INyz+INzx;
    end
    
    NIN=find(IN==3);    
        
    if size(NIN,2)==0
        for k=1:size(h,1)
            h(k,:)=h(k,:)/norm(h(k,:));
        end
        n=[sum(h(:,1)) sum(h(:,2)) sum(h(:,3))];
        CarTh(i)=norm(A-a)*dot((a-A),n)/abs(dot((a-A),n));    
    else
        Th=sqrt(diag(Dist(NIN,:)*Dist(NIN,:)'));
        k=find(min(Th));
        CarTh(i)=min(Th)*dot((a-A),h(NIN(k),:))/abs(dot((a-A),h(NIN(k),:)));
    end

    clear I J h CON Dist x IN NIN
end

% fid_vet=fopen([OUT,'.vet'],'w');
% fid_suf=fopen([OUT,'.suf'],'w');
% fprintf(fid_vet,'%10.7f %10.7f %10.7f %10.7f\n',[CarVert';CarTh]);
% fprintf(fid_suf,'%10.7f %10.7f %10.7f\n',CarSurf');
% fclose('all');


% 
% x=[CarVert(CarSurf(100,1),1);CarVert(CarSurf(100,2),1);CarVert(CarSurf(100,3),1);CarVert(CarSurf(100,1),1)];
% y=[CarVert(CarSurf(100,1),2);CarVert(CarSurf(100,2),2);CarVert(CarSurf(100,3),2);CarVert(CarSurf(100,1),2)];
% z=[CarVert(CarSurf(100,1),3);CarVert(CarSurf(100,2),3);CarVert(CarSurf(100,3),3);CarVert(CarSurf(100,1),3)];
% 
% x1=[CarVert(CarSurf(100,1),1);CarVert(CarSurf(100,1),1)+VertNorm(1222,1)];
% y1=[CarVert(CarSurf(100,1),2);CarVert(CarSurf(100,1),2)+VertNorm(1222,2)];
% z1=[CarVert(CarSurf(100,1),3);CarVert(CarSurf(100,1),3)+VertNorm(1222,3)];
% 
% x2=[CarVert(CarSurf(100,2),1);CarVert(CarSurf(100,2),1)+VertNorm(1261,1)];
% y2=[CarVert(CarSurf(100,2),2);CarVert(CarSurf(100,2),2)+VertNorm(1261,2)];
% z2=[CarVert(CarSurf(100,2),3);CarVert(CarSurf(100,2),3)+VertNorm(1261,3)];
% 
% x3=[CarVert(CarSurf(100,3),1);CarVert(CarSurf(100,3),1)+VertNorm(1247,1)];
% y3=[CarVert(CarSurf(100,3),2);CarVert(CarSurf(100,3),2)+VertNorm(1247,2)];
% z3=[CarVert(CarSurf(100,3),3);CarVert(CarSurf(100,3),3)+VertNorm(1247,3)];
% 
% tt=ones(size(CarVert,1),1);
% trimesh(CarSurf,CarVert(:,1),CarVert(:,2),CarVert(:,3),tt)
% hold on
% plot3(x,y,z)
% hold on
% plot3(x1,y1,z1,'r',x2,y2,z2,'b',x3,y3,z3,'k')
% rotate3d on
% 


disp('done');
disp(Bad_Points)
out = [CarVert';CarTh]; 

disp('End function closestDT')
end