function fpath = write_asc(polys, V, normals, fpath, title)
%write_asc  Write SIMM bone file.
%
%   Syntax:
%    fpath = write_asc(polys, V, normals, fpath, title)
%
%   Input:
%    polys:   M-by-1 cell array containing row vectors that define
%             polygons. Each row vector defines one polygon in the bone
%             model by listing row indices into V.
%    V:       N-by-3 array containing vertex coordinates. The rows
%             correspond to different vertices; the first, second and third
%             columns contain X-, Y- and Z-coordinates respectively.
%    normals: N-by-3 array containing vertex normals. The rows correspond
%             to different vertices; the first, second and third columns
%             contain X-, Y- and Z-components respectively.
%    fpath:   String containing a path to the file to which the bone data
%             are to be written. If set to an empty string, the path will
%             be obtained through a file output dialog. Optional, defaults
%             to an empty string.
%    title:   String containing the window title for the file output
%             dialog. Only used when fpath is set to an empty string.
%             Optional, defaults to 'Save SIMM bone file' if omitted or
%             empty.
%
%   Output:
%    fpath: String containing a path to the file that was written. Will be
%           set to an empty string if the user closes the file save dialog
%           box.
%
%   Effect: This function will write a geometrical model to a SIMM bone
%   file. The model is provided as a set of vertices and a set of polygons
%   that use these vertices as corner points.
%
%   Dependencies: none

%Created on 09/07/2008 by Ward Bartels.
%WB, 22/04/2010: Added handling of non-finite V and normals.
%Stabile, fully functional.


%Set defaults for input variables
if nargin<4, fpath = ''; end
if nargin<5 || isempty(title), title = 'Save SIMM bone file'; end

%If no file path was provided, get file from file input dialog
if isempty(fpath)
    [filename, pathname] = uiputfile({'*.*' 'SIMM bone file (*.*)'}, title);
    if isequal(filename, 0)
        fpath = '';
        return
    end
    fpath = fullfile(pathname, filename);
end

%Store line break character sequence
br = sprintf('\r\n');

%Make sure vertices and normals are valid numbers
V(~isfinite(V)) = 0;
normals(~isfinite(normals)) = 0;

%Create header string
header = sprintf(['NORM_ASCII' br '%.0f %.0f'], size(V, 1), numel(polys));

%Create bounding box string
box = [min(V, [], 1); max(V, [], 1)];
box = sprintf([br '%.6f %.6f %.6f %.6f %.6f %.6f'], box);

%Create vertices string
vertices = sprintf([br '%.7f %.7f %.7f %.7f %.7f %.7f'], [V normals].');

%Create polygon string
polys = [repmat({0}, size(polys)) num2cell(cellfun(@numel, polys)+1) polys].';
polys = sprintf(' %d', [polys{:}]-1); %apply zero-based index
polys = strrep(polys, ' -1 ', br);    %' -1 ' generated from {0} above

%Catch errors while writing string, attempt to close file if one was caught
try
    fid = fopen(fpath, 'w');
    fwrite(fid, [header box vertices polys], 'uchar');
    fclose(fid);
catch
    err = lasterror;
    try
        fclose(fid);
    end
    rethrow(err);
end