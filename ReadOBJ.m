function [Vertex,Surface]=ReadOBJ(File)

[h,x,y,z]=textread(File, '%c %q %q %q');
Coord=[x,y,z];

V=find(h=='v');
F=find(h=='f');

Vertex=Coord(V,:);
Surface=Coord(F,:);

Vertex=str2double(Vertex);
Surface=str2double(Surface);
