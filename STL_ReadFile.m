function [F, V, N] = STL_ReadFile(input_file_name,varargin)
%STL_ReadFile imports geometry from an STL file into MATLAB.
% Imports triangular faces from the ASCII or binary STL file indicated
% by input_file_name, and returns the faces, vertices and normals.
%
% USAGE: [F, V, N] = STL_ReadFile(input_file_name,varargin);
%
% INPUT:
% input_file_name   - String - Mandatory - The filename of the STL file.
% stlformat         - String - Optional  - The format of the STL file:
%                                        'ascii' or 'binary'
%
% OUTPUT:
% F contains the vertex lists defining each triangle face [n x 3].
% V contains the vertices for all triangles [3*n x 3].
% N contains the normals for each triangle face [n x 3].
%
% EMBEDDED FUNCTIONS:
% IDstlformat
% stlbin_read
% stlascii_read
%
% To see plot the 3D surface use:
%   patch('Faces',F,'Vertices',V,'FaceVertexCData',c);
% or
%   plot3(V(:,1),V(:,2),V(:,3),'.');
%
% WJ Zevenbergen, KU Leuven FABER, April 2013

% Check input:
if nargin < 1 % If no file input, try to get one from the user.
    [file_name,data_folder] = uigetfile('*.stl','Please select STL-file');
    input_file_name = [data_folder,'/',file_name];
    [stlformat] = IDstlformat(input_file_name);
else
    % input_file_name => exist
    if ~exist(input_file_name,'file')
        error(['File ','%s',' not found.'], input_file_name);   
    end
    if nargin == 2;
        stlformat = lower(varargin{1});
    else
       [stlformat] = IDstlformat(input_file_name);
    end
end

% Read the STL-file
if strcmp(stlformat,'ascii')
    [F, V, N] = stlascii_read(input_file_name);
elseif strcmp(stlformat,'binary')
    [F, V, N] = stlbin_read(input_file_name);
else
    return
end

end %function READSTL
%==========================================================================

%==========================================================================
function [stlformat] = IDstlformat(input_file_name)
%IDSTLFORMAT Test whether an stl file is ascii or binary
%
% Based on code originally written by:
% Adam H. Aitkenhead, The Christie NHS Foundation Trust, March 2010
%
% Re-written by WJ Zevenbergen, April 2013 (KU Leuven FABER)

% Open the file:
fidIN = fopen(input_file_name,'r');
if fidIN == -1
    error('File could not be opened, check name')
end

% Check the file size first, since binary files MUST have a size of 84+(50*n)
fseek(fidIN,0,1);         % Go to the end of the file
fidSIZE = ftell(fidIN);   % Check the size of the file

if rem(fidSIZE-84,50) > 0 
  stlformat = 'ascii';
else
  % Files with a size of 84+(50*n), might be either ascii or binary...
    
  % Read first 80 characters of the file.
  % For an ASCII file, the data should begin immediately (give or take a few
  % blank lines or spaces) and the first word must be 'solid'.
  % For a binary file, the first 80 characters contains the header.
  % It is bad practice to begin the header of a binary file with the word
  % 'solid', so it can be used to identify whether the file is ASCII or
  % binary.
  fseek(fidIN,0,-1);        % Go to the start of the file
  firsteighty = fread(fidIN,80,'uchar=>char')';

  % Trim leading and trailing spaces:
  firsteighty = strtrim(firsteighty);

  % Take the first five remaining characters, and check if these are 'solid':
  firstfive = firsteighty(1:min(5,length(firsteighty)));

  % Double check by reading the last 80 characters of the file.
  % For an ASCII file, the data should end (give or take a few
  % blank lines or spaces) with 'endsolid <object_name>'.
  % If the last 80 characters contains the word 'endsolid' then this
  % confirms that the file is indeed ASCII.
  if strcmp(firstfive,'solid')
    fseek(fidIN,-80,1);     % Go to the end of the file minus 80 characters
    lasteighty = char(fread(fidIN,80,'uchar')');
    if strfind(lasteighty,'endsolid')
      stlformat = 'ascii';
    else
      stlformat = 'binary';
    end
  else
    stlformat = 'binary';
  end
end

% Close the file
fclose(fidIN);
end %function
%==========================================================================

%==========================================================================
function [F, V, N] = stlascii_read(input_file_name)
%STLASCII_READ reads graphics information from an ASCII StereoLithography file.
%
% STL ascii file format
%   ASCII STL files have the following structure.  Technically each facet
%   could be any 2D shape, but in practice only triangular facets tend to be
%   used.  The present code ONLY works for meshes composed of triangular
%   facets.
%
% solid object_name
% facet normal x y z
%   outer loop
%     vertex x y z
%     vertex x y z
%     vertex x y z
%   endloop
% endfacet
% <Repeat for all facets...>
% endsolid object_name
%
% USAGE: [F, V, N] = stlascii_read(input_file_name);
%
% F contains the vertex lists defining each triangle face [n x 3].
% V contains the vertices for all triangles [3*n x 3].
% N contains the normals for each triangle face [n x 3].
%
% Based on code originally written by:
% Bill Seufzer, NASA Langley, February 2007
%
% Re-written by WJ Zevenbergen, April 2013 (KU Leuven FABER)

%  Open the file.
fidASCII = fopen(input_file_name,'r');
if fidASCII == -1
    error('File could not be opened, check name')
end

%  Allocate space.
solid = 0;
node = 0;
face = 0;

F = [];
V = [];
N = [];

% Read the text, one line at a time.
tic % measure time reading stl-file
while (1)
    text = fgetl(fidASCII);
    % On end of input, FGETL returns a -1 instead of a character array.
    if (~ischar(text))
        break
    end
    % Left justify the string (no initial blanks).
    % Lowercase the string.
    text = lower(strjust (text,'left'));
    % Locate the first word by finding the indices of the whitespace characters.
    blanks = find (isspace(text)==1);
    
    if (isempty(blanks))
        word1 = text;
    else
        word1 = text(1:blanks(1)-1);
    end
    % Action depends on initial keyword.
    switch word1
        case 'solid'   
            solid = solid + 1;     
        case 'facet'  
            face = face + 1;
            N(face,:) = sscanf (text, '%*s %*s %f %f %f')';   
        case 'vertex'   
            node = node + 1;
            V(node,:) = sscanf (text, '%*s %f %f %f')';    
        case 'endloop'  
            F(face,:) = [node-2, node-1, node];    
        otherwise         
    end  
end

% Close the file.
fclose (fidASCII);
if isempty(F);
    warning('No data in STL file');
    return
end
readingtime = toc;
fprintf('STL reading time: %8.2f seconds\n',readingtime)
end % function
%==========================================================================

%==========================================================================
function [F, V, N] = stlbin_read(input_file_name)
%STLBIN_READ This function reads an STL file in binary format into vertex 
% and face matrices V and F.
%
% STL binary file format
%   Binary STL files have an 84 byte header followed by 50-byte records, each
%   describing a single facet of the mesh.  Technically each facet could be
%   any 2D shape, but that would screw up the 50-byte-per-facet structure, so
%   in practice only triangular facets are used.  The present code ONLY works
%   for meshes composed of triangular facets.
%
% HEADER:
% 80 bytes:  Header text
% 4 bytes:   (int) The number of facets in the STL mesh
%
% DATA:
% 4 bytes:  (float) normal x
% 4 bytes:  (float) normal y
% 4 bytes:  (float) normal z
% 4 bytes:  (float) vertex1 x
% 4 bytes:  (float) vertex1 y
% 4 bytes:  (float) vertex1 z
% 4 bytes:  (float) vertex2 x
% 4 bytes:  (float) vertex2 y
% 4 bytes:  (float) vertex2 z
% 4 bytes:  (float) vertex3 x
% 4 bytes:  (float) vertex3 y
% 4 bytes:  (float) vertex3 z
% 2 bytes:  Padding to make the data for each facet 50-bytes in length
%   ...and repeat for next facet... 
%
% USAGE: [F, V, N] = stlbin_read(input_file_name);
%
% F contains the vertex lists defining each triangle face [n x 3].
% V contains the vertices for all triangles [3*n x 3].
% N contains the normals for each triangle face [n x 3].
%
% Based on code originally written by:
% Francis Esmonde-White, May 2010
%
% Re-written by WJ Zevenbergen, May 2012 (ORL Nijmegen)

fidBIN = fopen(input_file_name,'r'); % Open the file, assumes STL Binary format.
if fidBIN == -1 
    error('File could not be opened, check name')
end

% Read stl-file
ftitle = fread(fidBIN,80,'uchar=>schar'); % Read file title
stltitle = char(ftitle');
numFaces = fread(fidBIN,1,'int32'); % Read number of Faces
if numFaces == 0
    warning('No data in STL file');
    return
end

fprintf('\nTitle: %s\n', stltitle);
fprintf('Filename: %s\n', input_file_name);
fprintf('Num Faces: %d\n', numFaces);

tic % measure time reading stl-file

T = fread(fidBIN,inf,'uint8=>uint8'); % read the remaining values
fclose(fidBIN);

% Each facet is 50 bytes
%  - Three single precision values specifying the face normal vector
%  - Three single precision values specifying the first vertex (XYZ)
%  - Three single precision values specifying the second vertex (XYZ)
%  - Three single precision values specifying the third vertex (XYZ)
%  - Two color bytes (possibly zeroed)

% 3 dimensions x 4 bytes x 4 vertices = 48 bytes for triangle vertices
% 2 bytes = color (if color is specified)

trilist = 1:48;

ind = reshape(repmat(50*(0:(numFaces-1)),[48,1]),[1,48*numFaces])+repmat(trilist,[1,numFaces]);
Tri = reshape(typecast(T(ind),'single'),[3,4,numFaces]);

N = squeeze(Tri(:,1,:))';
N = double(N);

V = Tri(:,2:4,:);
V = reshape(V,[3,3*numFaces]);
V = double(V)';

F = reshape(1:3*numFaces,[3,numFaces])';

readingtime = toc;
fprintf('STL reading time: %8.2f seconds\n',readingtime)
end % function
%==========================================================================