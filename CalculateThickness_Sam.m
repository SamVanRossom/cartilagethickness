%%% Calculate thickness maps 

%Sam Van Rossom

clear all; close all; clc

%% Define input
path.stl = 'C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\Matlab\Cartilage Thickness Package\FemurCartMeshes'

pi = {'AdDB','CoSm','DiVa','ElMa','Fake','FiBu','FrDG','GeGi','HaBo','IrSi','JoCR','KeTa','MaWe','TeHo','TiDe'}; 

n_subjects = size(pi,2); 

for p = 1:n_subjects
    
name_cartilage = [pi{p} '_FemurCart.stl'];
name_subchondral = [pi{p} '_FemurMesh.stl']; 
file_Cartilage = fullfile(path.stl,name_cartilage);
file_Subchondral = fullfile(path.stl,name_subchondral);

[Cart.F, Cart.V, Cart.N] = STL_ReadFile(file_Cartilage); 
[Fem.F, Fem.V, Fem.N] = STL_ReadFile(file_Subchondral); 
OutputName = 'Test';
[thicknessMesh] = closestDT(Cart,Fem,OutputName); 

%save thickness mesh
fulloutputpath = fullfile(path.stl,[pi{p} '_ThicknessMesh.mat']); 
save(fulloutputpath,'thicknessMesh'); 

display(['Thickness map for ' pi{p} ' calculated and saved!'])
clear thicknessMesh Cart Fem
end 

commando = 'shutdown -s'; 
% 
% system(char(commando)); 