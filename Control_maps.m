%%% Control ThicknessMap

clear all; close all; clc

path.input = 'C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\Matlab\Cartilage Thickness Package';

 pi = {'AdDB'}%,'CoSm','DiVa','ElMa','Fake','FiBu','FrDG','GeGi','IrSi','JoCR','TeHo'}; 
 
for p = 1:size(pi,2); 
path.full = fullfile(path.input, 'STL', pi{p}, [pi{p} '_ThicknessMesh.mat']); 
path.stl = fullfile(path.input, 'STL', pi{p}, 'cartilage.stl');

load(path.full); 
[Cart.F, Cart.V, Cart.N] = STL_ReadFile(path.stl); 

path.stl_m1 = fullfile(path.input, 'STL', pi{p}, 'M1.stl');
[M1.F, M1.V, M1.N] = STL_ReadFile(path.stl_m1); 


thicknessMesh2 = thicknessMesh';




figure; 
trimesh(Cart.F,thicknessMesh2(:,1),thicknessMesh2(:,2),thicknessMesh2(:,3),thicknessMesh2(:,4))
view([0 -90])
set(gca,'clim',[0 7])
shading interp
axis off; grid off
colorbar

hold on 
trimesh(M1.F,M1.V(:,1),M1.V(:,2),M1.V(:,3))










%calculate average thickness
Average_Thickness(1,p) = nanmean(thicknessMesh(4,:)); 

% clear thicknessMesh Cart thicknessMesh2
end 