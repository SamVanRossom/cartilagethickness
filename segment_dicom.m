% Segment_dicom: opens up a bunch of dicom images and stores them into a
% cell array for segmentation
clc


clear cpoints

slice_direction = 'sagit';

dicom_files=dir('*.dcm');
no_of_dicom_files=size(dicom_files,1);

%create a cell array for the total number of images in the stack
mr_sequence = cell(no_of_dicom_files,1);
mr_sequence2 = cell(no_of_dicom_files,1);


% show the middle image and resize image stack
middle_image=round(no_of_dicom_files/2);
mrdata=dicomread(dicom_files(middle_image).name);

% read in dicom header information
info=dicominfo(dicom_files(middle_image).name);
pixdim=double(info.Width);
% .MR files from CP subjects do not provide ReconstructionDiameter 
% so use pixel spacing directly
% FOV=double(info.ReconstructionDiameter);
% pixmm=FOV/pixdim;
pixmm = double(info.PixelSpacing(1));
% .MR files from CP subjects do not provide SpacingBetweenSlices
% so use SliceThickness instead
% slice_thickness = double(info.SpacingBetweenSlices);
slice_thickness = double(info.SliceThickness);

fig1=figure(1);
set(fig1,'Units','pixels');
set(fig1,'position',[1,1,600,600]);
set(fig1,'Pointer','crosshair')
imagesc(mrdata);
title('Select the area that you would like to segment...start from top left!')
axis image
colormap gray
k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = round(point1(1,1:2));            % extract x and y
point2 = round(point2(1,1:2));
subimage = mrdata(point1(2):point2(2),point1(1):point2(1));
close

% read in the dicom image sequence and the subimage sequence
for S=1:no_of_dicom_files
    newfilename=dicom_files(S).name;
    mrdata = dicomread(newfilename);

    % create subimage which is a zoomed region of the original MR image
    subimage = mrdata(point1(2):point2(2),point1(1):point2(1));

    % stack the MR sequences into sequence cell arrays
    mr_sequence{S} = mrdata;
    mr_sequence2{S}=subimage;
end
startingframe=1;

save point1 point1
save mr_sequence2 mr_sequence2
save mr_sequence mr_sequence
save startingframe startingframe
save slice_thickness slice_thickness
save pixmm pixmm
save slice_direction slice_direction

test2