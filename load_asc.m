function [ver,nor,tri,area,cen,line1]=load_asc(infile,inpath,scale);
%LOAD_ASC is used to open asc data files containing triangulations of a
%surface
% [ver,nor,tri,area,cen]=load_asc(infile,inpath,scale)
%
% INPUT
% -infile   String including the name of the surface mesh <name>.asc
% -inpath   String containing a directory in which the surface mesh is
%           located
% -scale    1-by-3 array defining X, Y and Z scale factors asc geometry
%
% OUTPUT
% -ver      N-by-3 array defining vertices. The rows correspond to
%           different vertices and the columns correspond to X-, Y- and Z-
%           coordinates. The elements are coordinate values.
% -nor      N-by-3 array containing vertex normals. The rows correspond
%           to different vertices; the first, second and third columns
%           contain X-, Y- and Z-components respectively.
% -tri      M-by-3 array defining a triangle mesh. The rows correspond to
%           different triangles and the columns correspond to the three
%           vertices that make up each triangle. The elements are row
%           indices into vertices.
% -area     M-by-1 array defining the triangle area.
% -cen      M-by-3 array defining the centers of the triangle mesh. The
%           rows correspond to different triangles and the columns 
%           correspond to X-, Y- and Z-coordinates of the center of the 
%           triangle. The elements are coordinate values.

narg = nargin;
if (narg==0);
    [infile, inpath]=uigetfile('*.asc','Select input file');
    if infile==0;
        disp('No file selected');
        return;
    end
    fid = fopen([inpath infile],'r');
elseif (narg>0)
    if ~strcmp(infile((end-3):end),'.asc') 
        infile = [infile,'.asc'];
    end
    if (narg>=2)
        fid = fopen([inpath infile],'r');
        if fid == -1;
            fid = fopen(fullfile(inpath,infile),'r');
        end
    else 
        inpath='';
        fid=fopen(infile,'r');
    end
    if (narg < 3)
        scale = [1 1 1];
    elseif (length(scale) ~= 3)
        error('scale must be length 3')
    end
end
file = infile;

if (fid==-1);
    disp(['File not found: ',file]);
    anc=[];
    return;
end

%disregard header info
hdr=fgetl(fid);

% Now read in the number of vertices and triangles
line=fgetl(fid);
data=sscanf(line,'%f',2);
nver=data(1);
ntri=data(2);

% read first line
line1=fgetl(fid);

% Now read in the vertices
ver=zeros(nver,3);
nor=zeros(nver,3);
for i=1:nver
    data=fscanf(fid,'%f',6);
    ver(i,1:3)=data(1:3)'.*scale;
    nor(i,1:3)=data(4:6)';
end

% Now read in the triangles, add 1 since they are indexed from 0
tri=zeros(ntri,3);
for i=1:ntri
    data=fscanf(fid,'%f',4);
    tri(i,1:3)=data(2:4)'+1;
end


% Compute the areas of the triangles using heron's formula
area=[];
s1=ver(tri(:,2),:)-ver(tri(:,1),:);
s1=sqrt(sum(s1.^2,2));
s2=ver(tri(:,3),:)-ver(tri(:,2),:);
s2=sqrt(sum(s2.^2,2));
s3=ver(tri(:,1),:)-ver(tri(:,3),:);
s3=sqrt(sum(s3.^2,2));
s=(s1+s2+s3)/2;
area=sqrt(s.*(s-s1).*(s-s2).*(s-s3));

% Compute the center of each triangle
cen=[];
cen=(ver(tri(:,1),:)+ver(tri(:,2),:)+ver(tri(:,3),:))/3;

% Close file handle
fclose(fid);
