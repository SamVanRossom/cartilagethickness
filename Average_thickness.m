%%% Calculate average Thickness

clear all; close all; clc

%% Define input

path.in = 'C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\Matlab\Cartilage Thickness Package\STL';

pi ={'AdDB','CoSm','DiVa','ElMa','FaKe','Fibu','FrDG','GeGi','IrSi','JoCR','TeHo'};

for p = 1:size(pi,2);
    
    %load thicknessmesh
    path.mesh = fullfile(path.in,[pi{p} '_ThicknessMesh.mat']);
    load(path.mesh);
    
    %load faces
    path.faces = fullfile(path.in,pi{p},[pi{p} '_ZonalFaces.mat']);
    load(path.faces);
    
    %load STL of cartilage
    path.stl = fullfile(path.in,pi{p},'cartilage.stl');
    [Cart.F, Cart.V, Cart.N]= STL_ReadFile(path.stl);
    face_sel.med = face_sel.m1 + face_sel.m2 + face_sel.m3 + face_sel.m4 + face_sel.m5; 
    face_sel.med(face_sel.med>1) = 1; 
    
    face_sel.med = logical(face_sel.med); 
    face_sel.lat = (face_sel.med == 0); 
    face_sel.all = zeros(size(face_sel.m1)); 
    face_sel.all(:,:) = 1; face_sel.all = logical(face_sel.all);
    
%     figure; 
%     h = trimesh(Cart.F(face_sel.med,:),Cart.V(:,1),Cart.V(:,2),Cart.V(:,3));
%     set(h,'EdgeColor','k'); 
%     hold on
%      h = trimesh(Cart.F(face_sel.lat,:),Cart.V(:,1),Cart.V(:,2),Cart.V(:,3));
%     set(h,'EdgeColor','r'); 
    %% Calculations
    zones = fieldnames(face_sel);
    
    for z = 1:size(zones,1)
        VertexZonal.(zones{z}) =  Cart.F(face_sel.(zones{z}),:);
        Vertex_Sel.(zones{z}) = unique(VertexZonal.(zones{z}));
        
        Thickness_sel.(zones{z}) = thicknessMesh(4,Vertex_Sel.(zones{z}));
        
        
        MeanThickness(z,p) = nanmean(Thickness_sel.(zones{z}));
        
        %calculate max thickness as average of highest 10%
        ranked.(zones{z}) = sort(Thickness_sel.(zones{z}),'descend');
        nfaces = size(ranked.(zones{z}),2);
        nstop = round(0.1*nfaces);
        MaxThickness(z,p) = nanmean(ranked.(zones{z})(1,1:nstop));
        
    end %loop over zones
    
    clear Cart thicknessMesh face_sel VertexZonal Vertex_Sel Thickness_sel ranked
end %loop over patients

Thickness.Mean = MeanThickness; 
Thickness.Max = MaxThickness; 

% save(fullfile(path.in,'Thickness.mat'),'Thickness'); 
