function [Normals,Vertices,Faces] = VTP_ReadFile(filename)
%VTP_ReadFile - Read data from OpenSim vtp-file including geometry
%
%   Syntax:
%    [Normals,Vertices,Faces] = VTP_ReadFile(filename)
%
%   Input:
%    filename:  Name of the vtp-file
%
%   Output:
%    Normals:   M x 3 double array defining normalized triangle normals.
%               Optional, calculated automatically if omitted or empty.
%                Rows:     Mesh triangles
%                Columns:  X-, Y- and Z-components
%                Contents: Vector components
%    Vertices:  N x 3 double array defining vertices.
%                Rows:     Mesh vertices
%                Columns:  X-, Y- and Z-axes
%                Contents: Coordinate values
%    Faces:     M x 3 double array defining mesh.
%                Rows:     Mesh triangles
%                Columns:  Triangle vertices
%                Contents: Row indices into V
%
% WJ Zevenbergen, KU Leuven FABER, Februari 2014

% Check input:
if nargin < 1 % If no file input, try to get one from the user.
    [file_name,data_folder] = uigetfile('*.vtp','Please select VTP-file');
    filename = [data_folder,'/',file_name];
else
    % input_file_name => exist
    if ~exist(filename,'file')
        error(['File ','%s',' not found.'], filename);
    end
end

fidIN = fopen(filename,'r');
if fidIN == -1
    error('File could not be opened, check name');
end

str = fgetl(fidIN);   % -1 if eof; first line
str = fgetl(fidIN);   % 2nd line, check VTK
if ~strcmp(str(2:4),'VTK')
    error('The file is not a valid VTP one.');
end

% read header look for Piece
while (1)
    str = fgetl(fidIN);
    if strfind(str,'Piece')
        break
    end
end
k = strfind(str,'"');
NumberOfPoints = str2num(str(k(1)+1:k(2)-1));
NumberOfPolys = str2num(str(k(end-1)+1:k(end)-1));

% read normals
while (1)
    str = fgetl(fidIN);
    if strfind(str,'PointData')
        str = fgetl(fidIN);
        break
    end
end
[A,cnt] = fscanf(fidIN,'%f %f %f', 3*NumberOfPoints);
if cnt~=3*NumberOfPoints
    warning('Problem in reading normals');
end
Normals = reshape(A, 3, cnt/3)';

% read vertices
while (1)
    str = fgetl(fidIN);
    if strfind(str,'Points')
        str = fgetl(fidIN);
        break
    end
end
[A,cnt] = fscanf(fidIN,'%f %f %f', 3*NumberOfPoints);
if cnt~=3*NumberOfPoints
    warning('Problem in reading vertices');
end
Vertices = reshape(A, 3, cnt/3)';

% read faces
while (1)
    str = fgetl(fidIN);
    if strfind(str,'Polys')
        str = fgetl(fidIN);
        break
    end
end
[A,cnt] = fscanf(fidIN,'%f %f %f', 3*NumberOfPolys);
if cnt~=3*NumberOfPolys
    warning('Problem in reading faces');
end
Faces = reshape(A, 3, cnt/3)'+1;

fclose(fidIN);