clear all
close all
clc

CAR = 'cartilage.stl'
[CarSurf,CarVert,N_cart]= STL_ReadFile(CAR);

% load('AdDB_T1rhoMap.mat')
load('Map.mat')

test2 = nanmean(nanmean(T1map,3),1)
for i = 1:20
%     test(:,i) = test2(:,:,i); 
    test5(i,:) = test2(1,:); 
end
Test = test5; 
test3 = [Test(:,1);Test(:,2);Test(:,3);Test(:,4);Test(:,5);Test(:,6);Test(:,7);Test(:,8);Test(:,9);Test(:,10);Test(:,11);Test(:,12);Test(:,13);Test(:,14);...
    Test(:,15);Test(:,16);Test(:,17);Test(:,18);Test(:,19);Test(:,20)]; 
    
N = length(CarVert)./20; 
nf = size(CarVert,1)+525

test = test3;
% for i = 1:size(test,2)
%     
%    Test(:,i) = spline([1:size(test,1)]',test(:,i),[1:((size(test,1))/N):size(test,1)]);
%     
% end
% test3 = [Test(:,1);Test(:,1);Test(:,1);Test(:,1);Test(:,2);Test(:,2);Test(:,2);Test(:,2);Test(:,2);Test(:,4);Test(:,4);Test(:,4);Test(:,4);Test(:,4);...
%     Test(:,4);Test(:,16);Test(:,16);Test(:,16);Test(:,16);Test(:,16)]; 

% T1map = T1rhoexportable;
% clear T1rhoexportable
% a = 20;
% b = 60;
% Test_norm = (b-a).*rand(1,length(CarVert)) + a;
% Test_norm = randn([1,length(CarVert)]);

for i = 1:size(test,2)
    
   test4(:,i) = spline([1:size(test,1)]',test3(:,i),[1:((size(test,1))/nf):size(test,1)]);
    
end
figure
trisurf(CarSurf,CarVert(:,1),CarVert(:,2),CarVert(:,3),test4);
shading interp

 
%     A(i,:) = test(:,:,i);
% end
% clims = [1 100]
% figure
% surface(test)
