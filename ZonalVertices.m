%% Define Zonal Vertices 

% currently only medial side!!!!

clear all; close all; clc

%% Define input

pi = 'FiBu';

path.input = 'C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\Matlab\Cartilage Thickness Package\STL'; 

%meshname femur cartilage
name.femur = 'cartilage_redo.stl'; 

% Mesh-names of the different medial parts.
name.fm1 = 'M1.stl';    
name.fm2 = 'M2.stl'; 
name.fm3 = 'M3.stl'; 
name.fm4 = 'M4.stl'; 
name.fm5 = 'M5.stl'; 


%% Load Meshes
path.m1  = fullfile(path.input,pi,name.fm1); 
path.m2  = fullfile(path.input,pi,name.fm2); 
path.m3  = fullfile(path.input,pi,name.fm3); 
path.m4  = fullfile(path.input,pi,name.fm4); 
path.m5  = fullfile(path.input,pi,name.fm5); 
path.femur = fullfile(path.input,pi,name.femur); 
% 
% [V.femur,N.femur,F.femur,area.femur,cen.femur,line1.femur] = load_asc(path.femur,'',[1 1 1]);
% [V.fm1,N.fm1,F.fm1,area.fm1,cen.fm1,line1.fm1] = load_asc(path.m1,'',[1 1 1]);
% [V.fm2,N.fm2,F.fm2,area.fm2,cen.fm2,line1.fm2] = load_asc(path.m2,'',[1 1 1]);
% [V.fm3,N.fm3,F.fm3,area.fm3,cen.fm3,line1.fm3] = load_asc(path.m3,'',[1 1 1]);
% [V.fm4,N.fm4,F.fm4,area.fm4,cen.fm4,line1.fm4] = load_asc(path.m4,'',[1 1 1]);
% [V.fm5,N.fm5,F.fm5,area.fm5,cen.fm5,line1.fm5] = load_asc(path.m5,'',[1 1 1]);

[F.femur,V.femur,N.femur] = STL_ReadFile (path.femur);
[F.fm1,V.fm1,N.fm1] = STL_ReadFile (path.m1);
[F.fm2,V.fm2,N.fm2] = STL_ReadFile (path.m2);
[F.fm3,V.fm3,N.fm3] = STL_ReadFile (path.m3);
[F.fm4,V.fm4,N.fm4] = STL_ReadFile (path.m4);
[F.fm5,V.fm5,N.fm5] = STL_ReadFile (path.m5); 

%correct for rounding
V.femur = round(V.femur.*10000)./10000; 
V.fm1 = round(V.fm1.*10000)./10000; 
V.fm2 = round(V.fm2.*10000)./10000; 
V.fm3 = round(V.fm3.*10000)./10000; 
V.fm4 = round(V.fm4.*10000)./10000; 
V.fm5 = round(V.fm5.*10000)./10000; 


% name.fm1 = 'M1.asc'; 
% path.m1  = fullfile(path.input,pi,name.fm1); 
% [Va.fm1,Na.fm1,Fa.fm1,area.fm1,cen.fm1,line1.fm1] = load_asc(path.m1,'',[1 1 1]);

% figure; 
% subplot(121)
% trimesh(F.fm1,V.fm1(:,1),V.fm1(:,2),V.fm1(:,3))
% 
% subplot(122)
% trimesh(Fa.fm1,Va.fm1(:,1),Va.fm1(:,2),Va.fm1(:,3))

%% Define equal vertices

nV.femur = size(V.femur,1); 

nV.fm1 = size(V.fm1,1); 
nV.fm2 = size(V.fm2,1); 
nV.fm3 = size(V.fm3,1); 
nV.fm4 = size(V.fm4,1); 
nV.fm5 = size(V.fm5,1); 

% nV.fl1 = size(V.fl1,1); 
% nV.fl2 = size(V.fl2,1); 
% nV.fl3 = size(V.fl3,1); 
% nV.fl4 = size(V.fl4,1); 
% nV.fl5 = size(V.fl5,1); 


figure
subplot(121)
 h = trimesh(F.femur,V.femur(:,1),V.femur(:,2),V.femur(:,3)); 
view(36,10)
hold on  
h1 = trimesh(F.fm1,V.fm1(:,1),V.fm1(:,2),V.fm1(:,3)); 
h2 = trimesh(F.fm2,V.fm2(:,1),V.fm2(:,2),V.fm2(:,3)); 
h3 = trimesh(F.fm3,V.fm3(:,1),V.fm3(:,2),V.fm3(:,3)); 
h4 = trimesh(F.fm4,V.fm4(:,1),V.fm4(:,2),V.fm4(:,3)); 
h5 = trimesh(F.fm5,V.fm5(:,1),V.fm5(:,2),V.fm5(:,3)); 
 grid off
axis on
 set(h,'EdgeColor','k'); 
set(h1,'EdgeColor','r'); 
set(h2,'EdgeColor','b'); 
set(h3,'EdgeColor','g'); 
set(h4,'EdgeColor','m'); 
set(h5,'EdgeColor','c');
title('medial meshes')

vertices.m1 = ismember(V.femur,V.fm1,'rows');
face_sel.m1 = vertices.m1(F.femur);                      
if isequal(size(face_sel.m1), [3 1]);
    face_sel.m1 = face_sel.m1.';            
end
face_sel.m1 = any(face_sel.m1,2);  

vertices.m2 = ismember(V.femur,V.fm2,'rows');
face_sel.m2 = vertices.m2(F.femur);                       
if isequal(size(face_sel.m2), [3 1]);
    face_sel.m2 = face_sel.m2.';            
end
face_sel.m2 = any(face_sel.m2,2);  

vertices.m3 = ismember(V.femur,V.fm3,'rows');
face_sel.m3 = vertices.m3(F.femur);                       
if isequal(size(face_sel.m3), [3 1]);
    face_sel.m3 = face_sel.m3.';            
end
face_sel.m3 = any(face_sel.m3,2);  

vertices.m4 = ismember(V.femur,V.fm4,'rows');
face_sel.m4 = vertices.m4(F.femur);                       
if isequal(size(face_sel.m4), [3 1]);
    face_sel.m4 = face_sel.m4.';            
end
face_sel.m4 = any(face_sel.m4,2);  

vertices.m5 = ismember(V.femur,V.fm5,'rows');
face_sel.m5 = vertices.m5(F.femur);                       
if isequal(size(face_sel.m5), [3 1]);
    face_sel.m5 = face_sel.m5.';            
end
face_sel.m5 = any(face_sel.m5,2);  


subplot(122)
view(36,10)
h = trimesh(F.femur,V.femur(:,1),V.femur(:,2),V.femur(:,3)); 
hold on 

ht1 = trimesh(F.femur(face_sel.m1,:),V.femur(:,1),V.femur(:,2),V.femur(:,3));
ht2 = trimesh(F.femur(face_sel.m2,:),V.femur(:,1),V.femur(:,2),V.femur(:,3));
ht3 = trimesh(F.femur(face_sel.m3,:),V.femur(:,1),V.femur(:,2),V.femur(:,3));
ht4 = trimesh(F.femur(face_sel.m4,:),V.femur(:,1),V.femur(:,2),V.femur(:,3));
ht5 = trimesh(F.femur(face_sel.m5,:),V.femur(:,1),V.femur(:,2),V.femur(:,3));
view(36,10) 
set(h,'EdgeColor','k');
set(ht1,'EdgeColor','r')
set(ht2,'EdgeColor','b')
set(ht3,'EdgeColor','g')
set(ht4,'EdgeColor','m')
set(ht5,'EdgeColor','c')
 grid off
axis on

path.out = fullfile(path.input, pi, [pi '_ZonalFaces.mat']);

% save(path.out,'face_sel');
