function VData = faceColor2Vertice(faces,vertices,bodycP,center)
%Given a trirep and a body-centric CData, calculate the CData for each
%vertex
% INPUT
% -faces    M-by-3 array defining a triangle mesh. The rows correspond to
%           different triangles and the columns correspond to the three
%           vertices that make up each triangle. The elements are row
%           indices into vertices.
% -vertices N-by-3 array defining vertices. The rows correspond to
%           different vertices and the columns correspond to X-, Y- and Z-
%           coordinates. The elements are coordinate values.
% -bodycP   M-by-1 array defining the body center of pressure value. The
%           rows correspond to the different triangles.
% -center   M-by-3 array defining the centers of the triangle mesh. The
%           rows correspond to different triangles and the columns 
%           correspond to X-, Y- and Z-coordinates of the center of the 
%           triangle. The elements are coordinate values.
% OUTPUT
% -VData    N-by-1 array defining the vertex-centered pressure data.
%
% Lianne Zevenbergen, KU Leuven 2015

[vertices, ~, indexn] = unique(vertices,'rows');
faces = indexn(faces);

vertex2centers = vert2cent(faces,vertices,center);

k = size(vertices,1);
V = zeros(k,1);
for i = 1:k
    % find triangles that share vertex
    [triangles_of_interest,~] = find(faces == i);
    
    %grab the body-centered values of interest
    BodyPressure = bodycP(triangles_of_interest);
    
    %calculate the vertex-centered values of interest
    V(i) = sum(BodyPressure'.*vertex2centers{i})./sum(vertex2centers{i});
end
VData = V(indexn);

function vertex2centers = vert2cent(faces,vertices,center)
%Calculate the distance of vertices to the centers of the triangles in
%which the vertix is a member. 
%This is used in the loading function and is SLOW
% INPUT
% -faces    M-by-3 array defining a triangle mesh. The rows correspond to
%           different triangles and the columns correspond to the three
%           vertices that make up each triangle. The elements are row
%           indices into vertices.
% -vertices N-by-3 array defining vertices. The rows correspond to
%           different vertices and the columns correspond to X-, Y- and Z-
%           coordinates. The elements are coordinate values.
% -center   M-by-3 array defining the centers of the triangle mesh. The
%           rows correspond to different triangles and the columns 
%           correspond to X-, Y- and Z-coordinates of the center of the 
%           triangle. The elements are coordinate values.
% OUTPUT
% - vertex2centers  N-by-1 cell array defining the distances of the
%                   adjecent triangle centers.
%
% Lianne Zevenbergen, KU Leuven 2015

n = size(vertices,1);
vertex2centers=[];

%for all the given vertices
for j =1:n
    
    %find triangles which contain this vertex
    [triangles_of_interest,~] = find(faces == j);
    
    %get the centers for this triangle
    centers = center(triangles_of_interest,:);
    [m,~] = size(centers);
    
    %get the vertex location
    vertex=vertices(j,:);
    
    %for all the triangles which share this vertex
    for i = 1:m
        %compute the distance from the vertex to the center of the triangle
        vertex2centers{j}(i) = sqrt( (vertex(1)-centers(i,1)).^2 + (vertex(2)-centers(i,2)).^2 + (vertex(3)-centers(i,3)).^2 );
    end
end