%Read STL file
%
% Used functions
% - STL_ReadFile.m
% - write_asc.m
%
% Lianne Zevenbergen, april 2015

clear all, close all, clc
% select the stl-files in folder
[file_name,data_folder] = uigetfile('*.stl','Please select STL-file','MultiSelect','on');
%Stop if user closed dialog box
if data_folder == 0;
    return
end
if ~iscell(file_name)
    file_name_1 = file_name; clear file_name
    file_name{1,1} = file_name_1;
end

% loop through all the file's to convert
for j = 1:length(file_name)
    input_file_name = fullfile(data_folder,file_name{1,j});
    % read the stl-file <<STL_ReadFile.m>>
    [F, V, N] = STL_ReadFile(input_file_name);
    
    % stl is in mm the output asc for OpenSim is in m
    stl_mm = true;
    if stl_mm == 1;
        Vnew = V.*0.001;
    else
        Vnew = V;
    end
    
    % Calculate Vertex Normal
    TR = triangulation(F,Vnew);
    VN = vertexNormal(TR);
    
    %Change extension for fpath
    [fdir, fname] = fileparts(input_file_name);
    fpath = [fullfile(fdir, fname) '.asc'];
    
    %Write SIMM bone file <<write_asc.m>>
    write_asc(num2cell(F, 2), Vnew, VN, fpath);
end