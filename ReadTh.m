function [x,y,z,OrT,ReT,Per,SOD,Surf]=ReadTh(File_Ver,File_Suf,DispType,ShadType,Threshold)

% Minimum and maximum values for colorbar
minval = 0;
maxval = 7;

[x,y,z,OrT,ReT,Per,SOD]=textread(File_Ver, '%f %f %f %f %f %f %f');
[Surf_1,Surf_2,Surf_3]=textread(File_Suf, '%f %f %f');
Surf=[Surf_1,Surf_2,Surf_3];

figure(1)

switch DispType
    case '1'
        trisurf(Surf,x,z,y,OrT);
        set(gca,'clim',[minval maxval])
    case '2'
        trisurf(Surf,-x,z,y,ReT);
        caxis([-1.5 1.5]);
    case '3'
        trisurf(Surf,-x,z,y,Per);  
        caxis([-100 100]);
    case '4'
        for i=1:length(ReT)
            if ReT(i)<-str2double(Threshold)
                ContactArea(i)=0;
            else
                ContactArea(i)=1;
            end
        end
        trisurf(Surf,-x,z,y,ContactArea);
%         trisurf(Surf,-x,z,y,SOD);
end

axis equal;

switch ShadType
    case '1'
        shading faceted;
    case '2'
        shading flat;
    case '3'
        shading interp;
end

colorbar;  
grid on;
rotate3d on;
        

