%%% Calculate average Thickness

clear all; close all; clc

%% Define input

path.in = 'C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\Matlab\Cartilage Thickness Package\FemurCartMeshes';

pi ={'AdDB','CoSm','DiVa','ElMa','FaKe','Fibu','FrDG','GeGi','HaBo','IrSi','JoCR','KeTa','MaWe','TeHo','TiDe'};

for p = 1:size(pi,2);
    
    %load thicknessmesh
    path.mesh = fullfile(path.in,[pi{p} '_ThicknessMesh.mat']);
    load(path.mesh);
    
    %load faces
    path.faces = fullfile(path.in,[pi{p} '_WB_faces.mat']);
    load(path.faces);
    
    %load STL of cartilage
    path.stl = fullfile(path.in,[pi{p},'_FemurCart.asc']);
    [Cart.V,Cart.N,Cart.F,Cart.area,Cart.cen,Cart.line1] = load_asc(path.stl);
    
%     [Cart.F, Cart.V, Cart.N]= STL_ReadFile(path.stl);
%     face_sel.med = face_sel.m1 + face_sel.m2 + face_sel.m3 + face_sel.m4 + face_sel.m5;
%     face_sel.med(face_sel.med>1) = 1;
%     
%     face_sel.med = logical(face_sel.med);
%     face_sel.lat = (face_sel.med == 0);
% %     face_sel.all = zeros(size(face_sel.m1));
%     face_sel.all(:,:) = 1; face_sel.all = logical(face_sel.all);
   
%     figure;
%     h = trimesh(Cart.F,Cart.V(:,1),Cart.V(:,2),Cart.V(:,3));
%     set(h,'EdgeColor','k');
%     hold on
%     hlat = trimesh(Cart.F(face_sel.lat,:),Cart.V(:,1),Cart.V(:,2),Cart.V(:,3));
%     set(hlat,'EdgeColor','r');
%     hmed = trimesh(Cart.F(face_sel.med,:),Cart.V(:,1),Cart.V(:,2),Cart.V(:,3));
%     set(hmed,'EdgeColor','b');
%     
    %% Calculations
    zones = fieldnames(face_sel);
    
    for z = 1:size(zones,1)
        VertexZonal.(zones{z}) =  Cart.F(face_sel.(zones{z}),:);
        Vertex_Sel.(zones{z}) = unique(VertexZonal.(zones{z}));
        
        Thickness_sel.(zones{z}) = thicknessMesh(4,Vertex_Sel.(zones{z}));
        
        
        MeanThickness(z,p) = nanmean(Thickness_sel.(zones{z}));
        
        %calculate max thickness as average of highest 10%
        ranked.(zones{z}) = sort(Thickness_sel.(zones{z}),'descend');
        nfaces = size(ranked.(zones{z}),2);
        nstop = round(0.1*nfaces);
        MaxThickness(z,p) = nanmean(ranked.(zones{z})(1,1:nstop));
        
    end %loop over zones
    
    %calculate thickness per face.
    nfaces =  size(Cart.F,1);
    for f =  1:nfaces
        FaceThickness(f,1) = (nanmean(thicknessMesh(4,Cart.F(f,:))));
    end
    
%     FaceThickness((face_sel.med==0),:) = 0;
    face_sel.combinatie = face_sel.med + face_sel.lat;
    FaceThickness((face_sel.combinatie == 0),:) = 0;
    dVColormed = faceColor2Vertice(Cart.F,Cart.V,FaceThickness,Cart.cen );
    dVColor_sort = sort(dVColormed,'descend');
    dVColormed_cut = mean(dVColor_sort(1:round(length(find(dVColor_sort))/100)));
    cmap = colormap(jet(128));
    cmap(1,:) = [248/256,248/256,255/256];
    
    h1 = figure; clf(h1), hold on
    set(h1,'Colormap',cmap);
    set(gca,'CLim',[0, dVColormed_cut]);
    
    % pTibia = patch('Faces',Tibia.F,'Vertices',Tibia.V,'FaceColor',[238/256,223/256,204/256],'EdgeColor','none','FaceLighting','phong');
    pTibiaCar = patch('Faces',Cart.F,'Vertices',Cart.V);
    % set(pTibiaCar,'FaceColor','interp','FaceVertexCData',dVColormed,'EdgeColor','none','CDataMapping','scaled','FaceLighting','phong');
    set(pTibiaCar,'FaceColor','interp','FaceVertexCData',dVColormed,'EdgeColor','none','CDataMapping','scaled','FaceLighting','phong');
    
    colormap(gca,cmap);
    daspect([1 1 1])
    view([180 -90])
    camroll(180)
    axis vis3d; grid off
    axis off
    camlight headlight
    material([0.4 .6 .05]);
    hbar1 = colorbar; xlabel(hbar1,'mm','FontSize',14)
    set(gca,'FontSize',12);
    hold(gca,'off')
    
    clear Cart thicknessMesh face_sel VertexZonal Vertex_Sel Thickness_sel ranked
    
    
end

Thickness.Mean = MeanThickness;
Thickness.Max = MaxThickness;
save('C:\Users\u0093377\Documents\Projects\1-MRI_Pressure\NewZones\WB_Thickness.mat','Thickness');
save(fullfile(path.in,'Thickness.mat'),'Thickness');
